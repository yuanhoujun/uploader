#include <cppcms/application.h>
#include <cppcms/applications_pool.h>
#include <cppcms/service.h>
#include <cppcms/http_response.h>
#include <cppcms/url_dispatcher.h>
#include <cppcms/http_file.h>
#include <iostream>
#include "../data/content.h"
#include "../data/index.h"
#include "../data/app.h"
#include <sys/time.h>
#include <string>
#include <cppdb/frontend.h>
#include <vector>
#include <booster/shared_ptr.h>
#include <stdio.h>
#include <stdlib.h>
#include <cppcms/json.h>
#include <cppcms/cppcms_error.h>

using namespace std;

extern string currentTimeInMillis();
extern string getCurrentTime();

#include <sstream>
template <typename T>
string to_string(T value) {
  ostringstream os;
  os << value;
  return os.str();
}

class uploader: public cppcms::application {
public:
    uploader(cppcms::service &s) :
        cppcms::application(s) {
        // dispatcher().assign("",&uploader::list,this);
        // mapper().assign("");
        init();

        //默认页面设置为导航页面
        dispatcher().assign("",&uploader::index,this);
        mapper().assign("");

        //导航页面
        dispatcher().assign("/index",&uploader::index,this);
        mapper().assign("index","/index");
        //上传页面
        dispatcher().assign("/upload",&uploader::upload,this);
        mapper().assign("upload","/upload");
        //通过app上传图片
        dispatcher().assign("/app/upload",&uploader::upload_by_app,this);
        mapper().assign("app_upload","/app/upload");
        //分页展示图片
        dispatcher().assign("/list",&uploader::list,this);
        mapper().assign("list","/list");
        //app分页图片展示
        dispatcher().assign("/app/list",&uploader::get,this);
        mapper().assign("app_list","/app/list");
        //app使用说明页面
        dispatcher().assign("/app",&uploader::app,this);
        mapper().assign("app_intro","/app");

        //删除图片
        dispatcher().assign("/delete",&uploader::del,this);
        mapper().assign("delete","/delete");

        mapper().root("/uploader");
    }

    ~uploader() {
      session.close();
    }

    void init() {
      try {
        conn_str = settings().get<std::string>("mysql.connection_string");
        session.open(conn_str);
      } catch(std::exception const &e) {
        std::cout << "Database connect fail : " << e.what() << endl;
      }
      //basePath = request().http_host();
    }

    void index() {
      data::index index;
      index.basePath = request().http_host();
      render("nav",index);
    }

    void app() {
      data::app app;
      app.basePath = request().http_host();
      int index = app.basePath.find(":");
      app.ip = app.basePath.substr(0,index);
      app.port = app.basePath.substr(index + 1);
      render("app",app);
    }

    void upload()
    {
        content::upload c;
        if(request().request_method()=="POST") {
            c.info.load(context());
            try {
                std::string mime = c.info.image.value()->mime();
                std::string new_name = currentTimeInMillis() + "." + mime.substr(mime.find("/") + 1);
                std::string url = "http://" + request().http_host() + "/photos/" + new_name;
                std::string name = c.info.image.value()->filename();
                std::string upload_time = getCurrentTime();
                std::string size = to_string(c.info.image.value()->size());

                c.info.image.value()->save_to("./output/upload/" + new_name);

                add(url,name,upload_time,size,"通过网页测试上传");

                response().out() << "文件上传成功！" ;
            } catch(cppcms::cppcms_error const &e) {
                std::cout << ("文件上传失败: " + e.trace()) << std::endl;
                response().out() << "文件上传失败: 请先选择文件";
            }
        	  c.info.clear();
        }
       render("upload",c);
  }

  void list() {
    int num = 1;
    int pagesize = 5;

    string pagenum = request().get("pagenum");

    cout << "pagenum = " << pagenum << endl;

    if (!pagenum.empty()) {
      num = atoi(pagenum.c_str());
    }

    content::images imgs;

    cppdb::result res;
    cppdb::result res1;
    
    res1 = session << "select count(id) from t_photo";

    if (res1.next()) {
        int total_count = res1.get<int>(0);
        int pcount = total_count % pagesize == 0 ? total_count / pagesize : total_count / pagesize + 1;
        imgs.pagecount = to_string(pcount);
    }

    res = session << "select * from t_photo order by upload_time desc limit ?,?" << ((num - 1)* pagesize) << pagesize;
    while(res.next()) {
      photo p;
      res.fetch("id",p.id);
      res.fetch("name",p.name);
      res.fetch("url",p.url);
      res.fetch("upload_time",p.upload_time);
      res.fetch("size",p.size);
      res.fetch("description",p.description);
      res.fetch("upload_method",p.upload_method);

      // p.id = res.get<std::string>("id");
      // p.name = res.get<std::string>("name");
      // p.url = res.get<std::string>("url");
      // p.upload_time = res.get<std::string>("upload_time");
      // p.size = res.get<std::string>("size");
      // p.description = res.get<std::string>("description");
      // p.upload_method = res.get<std::string>("upload_method");

      // std::cout << ">>>>>" + res.get<std::string>("upload_time") << std::endl;
      // std::cout << ">>>>>" + res.get<std::string>("upload_method") << std::endl;
 
      imgs.photos.push_back(p);
    }

    imgs.pagenum = to_string(num);
    imgs.pagesize = to_string(pagesize);
    imgs.server = "http://" + request().http_host();

    render("imgs",imgs);
  }

  void del() {
    std::string id = request().get("id");
    
    std::cout << "id = " << id << endl;

    cppcms::json::value resObj;

    if (id.empty()) {
      resObj["code"] = "101";
      resObj["msg"] = "Image id is empty";
    } else {
      try {
      std::string sql = "delete from t_photo where id = ?";
      cppdb::transaction tr(session);
      cppdb::statement stat;
      stat = session << sql << id << cppdb::exec;
      unsigned long long row = stat.affected();
      if (row > 0) {
        resObj["code"] = "0";
        resObj["msg"] = "delete ok";
      } else {
        resObj["code"] = "102";
        resObj["msg"] = "Unknown image id";
      }
      tr.commit();
    } catch (std::exception const &e) {
      std::cout << e.what() << std::endl;
      resObj["code"] = "103";
      resObj["msg"] = "Database invoke fail";
    }
    }
    response().out() << resObj ;
  }

  void upload_by_app() {
     if(request().request_method()=="POST") {
          try {
              cppcms::http::request::files_type files = request().files();

              if (!files.empty()) {
                for (unsigned int i = 0; i != files.size() ; ++i) {
                    booster::shared_ptr<cppcms::http::file> file = files.at(i);

                    std::string mime = file->mime();
                    std::string new_name = currentTimeInMillis() + "." + mime.substr(mime.find("/") + 1);
                    std::string url = "http://" + request().http_host() + "/photos/" + new_name;
                    std::string name = file->filename();
                    std::string upload_time = getCurrentTime();
                    std::string size = to_string(file->size());

                    file->save_to("./output/upload/" + new_name);

                    add(url,name,upload_time,size,"通过APP上传");

                    response().out() << "{\"code\": 0,\"data\": {\"msg\":\"恭喜你，上传成功！\"}}";
                }
              } else {
                response().out() << "{\"code\": 101,\"data\": {\"msg\":\"未检测到上传文件\"}}";
              }
          } catch(cppcms::cppcms_error const &e) {
              std::cout << ("文件上传失败: " + e.trace()) << std::endl;
              // response().out() << ("文件上传失败: " + e.trace());
              response().out() << "{\"code\": 102,\"data\": {\"msg\":\"服务器繁忙，请稍后再试!\"}}";
          }
      } else {
          response().out() << "{\"code\": 103,\"data\": {\"msg\":\"只支持POST请求哦！\"}}";
      }
  }

  void get() {
    string pagenum = "";
    string pagesize = "";

    if (request().request_method() == "GET") {
       pagenum = request().get("pagenum");
       pagesize = request().get("pagesize");
    } else {
       pagenum = request().post("pagenum");
       pagesize = request().post("pagesize");
    }

    cout << "pagenum = " << pagenum << endl;
    cout << "pagesize = " << pagesize << endl;

    int num = atoi(pagenum.c_str());
    int size = atoi(pagesize.c_str());

    num = num <= 0 ? 1 : num;
    size = size <= 0 ? 5 : size;

    cppcms::json::value jsonObj;

    try {
        cppdb::result res = session << "select id,url from t_photo by upload_time desc limit ?,?" << ((num - 1) * size) << size ;
        int i = 0;
        while(res.next()) {
          cppcms::json::value jsonData;
          string id;
          string url;
          res.fetch("id",id);
          res.fetch("url",url);
          jsonData["id"] = id;
          jsonData["url"] = url;
          jsonObj["data"][i] = jsonData;
          i++;
        }
        jsonObj["code"] = "0";
        jsonObj["msg"] = "ok";
    } catch(std::exception const &e) {
        std::cout << e.what() << std::endl;
        jsonObj["code"] = "101";
        jsonObj["msg"] = "Database invoke error";
    }

    response().out() << jsonObj;
  }

  void add(std::string url,std::string name,std::string upload_time,std::string size,std::string upload_method) {
      cppdb::transaction tr(session);
      cppdb::statement stat;

      stat = session << "insert into t_photo(url,name,upload_time,size,upload_method) values(?,?,?,?,?)"
                     << url << name << upload_time << size << upload_method << cppdb::exec;

      tr.commit();
  }
private:
  cppdb::session session;       //session
  std::string conn_str;         //数据库连接字符串
 // std::string basePath;
  //int pagenum;
  //int pagesize;
};

string currentTimeInMillis() {
   struct timeval tv;       
   if(gettimeofday(&tv, NULL) != 0) return 0;
   unsigned long ctm = (unsigned long)((tv.tv_sec * 1000ul) + (tv.tv_usec / 1000ul));
   string new_name;
   stringstream ss;
   ss << ctm;
   ss >> new_name;
   return new_name;
}

string getCurrentTime() {
  time_t tt = time(NULL);
  tm* t = localtime(&tt);
  int y = t->tm_year + 1900;
  int m = t->tm_mon + 1;
  int d = t->tm_mday;
  int hour = t->tm_hour;
  int min = t->tm_min;
  int sec = t->tm_sec;

  string yStr = to_string(y);
  string mStr = to_string(m);
  string dStr = to_string(d);
  string hourStr = to_string(hour);
  string minStr = to_string(min);
  string secStr = to_string(sec);

  hourStr = hourStr.size() == 1 ? "0" + hourStr : hourStr;
  minStr = minStr.size() == 1 ? "0" + minStr : minStr;
  secStr = secStr.size() == 1 ? "0" + secStr : secStr;

  return yStr + "年" + mStr + "月" + dStr + "日 " + hourStr + ":" + minStr + ":" + secStr; 
}

int main(int argc,char ** argv)
{
    try {
        cppcms::service app(argc,argv);
        app.applications_pool().mount(cppcms::applications_factory<uploader>());

        int port = app.settings().get<int>("service.port");
        std::cout << "============================================" << std::endl;
        std::cout << "监听端口：" + to_string(port) << std::endl;
        std::cout << "============================================" << std::endl;
        
        app.run();
    } catch(std::exception const &e) {
        cerr<<e.what()<<endl;
    }
}
