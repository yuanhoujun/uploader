{
	"service" : {
		"api" : "http",
		"ip" : "10.211.55.4",
		"port" : 8080
	},
	"mysql" : {
		"connection_string" : "mysql:database=uploader;user=root;password=4016885;@pool_size=10;set_charset_name=utf8"
	},
	"http" : {
		"script" : "/uploader"
	},
	"file_server" : {
		"enable" : true,
		"listing" : false,
		"document_root" : ".",
		"alias" : [{
			"url" :"/photos","path" : "./output/upload"
		}]
	},
	"views" : {
		"default_skin" : "my_skin"
	}
}
