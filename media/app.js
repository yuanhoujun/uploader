$(function() {
	app.copy_upload_url();
	app.copy_list_url();
});

var app = {
	copy_upload_url : function() {
		$("#btn1").bind("click",function() {
			var upload_url = $("#url_upload_img").html();
			app.copyToClipboard('btn1',upload_url);
		});
	},
	copy_list_url: function() {
		$("#btn2").bind("click",function() {
			var list_url = $("#url_list_img").attr("href");
			app.copyToClipboard("btn2",list_url);
		});
	},
	copyToClipboard : function(id,value) {
		var clip = new ZeroClipboard.Client();
		clip.setHandCursor(true);
		clip.setText(value);
		clip.glue(id);
		clip.addEventListener("complete",function() {
			alert("复制成功");
		});
	}
};