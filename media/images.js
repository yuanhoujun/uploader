$(function() {
	image.delete();
});

var image = {
	delete : function() {
		$("a[del='ok']").bind("click",function() {
			var bool = window.confirm("确认删除吗？");
			if (bool) {
				var link = $(this).attr("link");
				var parentTr = $(this).parent().parent();
				$.ajax({
					url : link,
					type : "post",
					cache : false,
					success : function(res) {
						var res = eval('(' + res + ")");
						if (res.code == "0") {
							parentTr.hide();
						} else {
							alert("删除失败");
						}
					} 
				});
			}
		});
	}
};