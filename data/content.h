#ifndef CONTENT_H
#define CONTENT_H

#include <cppcms/view.h>
#include <cppcms/form.h>
#include <string>
#include <list>
#include "../apps/photo.h"

namespace content {
	// struct photos : public cppcms::base_content {
	// 	std::list<photo *> photos;
	// };
	struct upload_form : public cppcms::form {
		//cppcms::widgets::text description;
		cppcms::widgets::file image;
		cppcms::widgets::submit submit;

		upload_form() {
			//description.message("图片上传");

			image.message("选择图片");

			submit.value("上传");

			//add(description);
			add(image);
			add(submit);
		}
	};

	struct upload : public cppcms::base_content {
		upload_form info;
		std::string basePath;
	};

	struct photos : public cppcms::base_content {
		std::list<photo> photos;
	};
	struct images : public cppcms::base_content {
		std::string pagenum;
		std::string pagesize;
		std::string pagecount;
		std::string server;
		std::list<photo> photos;
	};
};

#endif